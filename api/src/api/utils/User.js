var mongoose = require("mongoose")
var TitleModel = require("./../models/Title")
var EntryModel = require("./../models/Entry")
var UserModel = require("./../models/User")
var config = require("./../../config")

mongoose.connect(config.db)

module.exports.create = function(user) {
    var newUser = UserModel(user)
    newUser.save()
}

module.exports.getById = function(id,next) {
    UserModel.findById(id,"-password").populate("entries").exec(function(err,data) {
        if (err) throw err
        next(data)
    })
}

module.exports.getAll = function(next) {
    UserModel.find({},"-password",function(err,data) {
        if (err) throw err
        next(data)
    })
}

module.exports.checkPassword = function(id,password,iftrue,iffalse) {
    UserModel.findById(id,function(err,data) {
        if (err)throw err
        if (data.password==password){
            iftrue()
        }else{
            iffalse()
        }
    })
}