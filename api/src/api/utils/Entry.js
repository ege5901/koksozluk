var mongoose = require("mongoose")
var TitleModel = require("./../models/Title")
var EntryModel = require("./../models/Entry")
var UserModel = require("./../models/User")
var config = require("./../../config")

mongoose.connect(config.db)

module.exports.create = function(entry,titleId) {
    var newEntryId = new mongoose.Types.ObjectId()
    entry._id = newEntryId
    entry.title= titleId
    var newEntry = new EntryModel(entry)
    newEntry.save()
    
    
    TitleModel.findById(titleId,function(err,data) {
        if (err) throw err
        data.entries.push(newEntryId)
        data.save()
    })
    UserModel.findById(entry.author,function(err,data) {
        if(err) throw err
        data.entries.push(entry._id)
        data.save()
    })
}