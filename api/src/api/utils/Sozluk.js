var mongoose = require("mongoose")
var TitleModel = require("./../models/Title")
var EntryModel = require("./../models/Entry")
var UserModel = require("./../models/User")
var SozlukModel = require("./../models/Sozluk")
var config = require("./../../config")

mongoose.connect(config.db)

module.exports.create = function(sozluk) {
    var newSozluk = SozlukModel(sozluk)
    newSozluk.save()
}

module.exports.getAll = function(next) {
    SozlukModel.find({}).exec(function(err,data) {
        if(err) throw err
        next(data)
    })
}

module.exports.getById=function(id,next){
    SozlukModel.findById(id,function(err,data) {
        if(err) throw err
        next(data)
    })
}