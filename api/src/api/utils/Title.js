var mongoose = require("mongoose")
var TitleModel = require("./../models/Title")
var EntryModel = require("./../models/Entry")
var UserModel = require("./../models/User")
var SozlukModel = require("./../models/Sozluk")
var config = require("./../../config")
var Entry = require("./../utils/Entry.js")

mongoose.connect(config.db)

module.exports.getAll = function(next){
    TitleModel.find({}).exec(function(err,data) {
        if (err) {
            throw err
        }
        next(data)
    })
}

module.exports.getById = function(id,next) {
    TitleModel.findById(id).populate("entries").exec(function(err,data) {
        if (err) throw err
        next(data)
    })
}

module.exports.create = function(title,entry) {
    //var entryId = new mongoose.Types.ObjectId()
    var titleId = new mongoose.Types.ObjectId()
    
    //entry._id = entryId
    //title.entries = [entryId]
    title._id = titleId
    //entry.title = titleId
    //entry.author = authorId
    //var newEntry = new EntryModel(entry)
    var newTitle = new TitleModel(title)
    //newEntry.save()
    newTitle.save(function() {
        Entry.create(entry,titleId)
        SozlukModel.findById(title.sozluk,function(err,data) {
            if (err) throw err
            data.titles.push(titleId)
            data.save()
        })
    })

}