var mongoose = require('mongoose');
var Schema = mongoose.Schema

var sozlukSchema = Schema({
    admin:{
        type:Schema.Types.ObjectId,
        ref:"User",
        required:true,
    },
    name: {
        type: String,
        required:true
    },
    titles:[{
        type:Schema.Types.ObjectId,
        ref:"Title",
    }],/*
    moderators:[{
        type:Schema.Types.ObjectId,
        ref:"User",
    }]*/
})

var Sozluk = mongoose.model("Sozluk",sozlukSchema)

module.exports=Sozluk