var mongoose = require('mongoose');
var Schema = mongoose.Schema

var titleSchema = Schema({
    name:{
        type:String,
        required:true,
    },
    entries:[{
        type: Schema.Types.ObjectId,
        ref:"Entry",
    }],
    sozluk:{
        type:Schema.Types.ObjectId,
        ref:"Sozluk",
        required:true
    }
})

var Title = mongoose.model("Title",titleSchema)

module.exports=Title