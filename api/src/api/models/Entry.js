var mongoose = require('mongoose');
var Schema = mongoose.Schema

var entrySchema = Schema({
    author:{
        type:Schema.Types.ObjectId,
        ref:"User",
        required:true,
    },
    content: {
        type: String,
        required:true
    },
    title:{
        type:Schema.Types.ObjectId,
        ref:"Title",
        required:true
    },
    date:Date
})

var Entry = mongoose.model("Entry",entrySchema)

module.exports=Entry