var mongoose = require('mongoose');
var Schema = mongoose.Schema

var userSchema = Schema({
    name:{
        type:String,
        required:true,
    },
    password:{
        type:String,
        required:true,
    },
    entries:[{
        type: Schema.Types.ObjectId,
        ref:"Entry",
    }],
})

var User = mongoose.model("User",userSchema)

module.exports=User