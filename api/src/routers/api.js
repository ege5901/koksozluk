var express = require("express")
var router = express.Router()
var Title = require("./../api/utils/Title")
var Entry = require("./../api/utils/Entry")
var User = require("./../api/utils/User")
var Sozluk = require("./../api/utils/Sozluk")
var bodyParser = require("body-parser");

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/',function(req,res){
    res.json({"msg" : 'hello world'});
});

router.post("/title",function(req,res) {
    Title.create(req.body.title,req.body.entry)
    res.end()
    
})

router.get('/titles',function(req,res){
    Title.getAll(function(data) {
        res.json(data)
    })
});

router.get("/title/:id",function(req,res) {
    Title.getById(req.params.id,function(data) {
        res.json(data)
    })    
})

router.post("/entry",function(req,res) {
    Entry.create(req.body.entry,req.body.titleId)
    res.end()
})

router.post("/user",function(req,res) {
    User.create(req.body.user)
    res.end()  
})

router.get("/user/:id",function(req,res) {
    User.getById(req.params.id,function(data) {
        res.json(data)
    })
})

router.get("/users",function(req,res) {
    User.getAll(function(data) {
        res.json(data)
    })
})

router.post("/sozluk",function(req,res) {
    Sozluk.create(req.body.sozluk)
    res.end()
})

router.get("/sozluks",function(req,res) {
    Sozluk.getAll(function(data) {
        res.json(data)
    })
})

router.get("/sozluk/:id",function(req,res) {
    Sozluk.getById(req.params.id,function(data) {
        res.json(data)
    })
})
module.exports = router