const express = require('express')
const app = express()
var bodyParser = require("body-parser");
const config = require("./config.js")
const api = require("./routers/api.js")

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) =>{
    res.send("hello")
})

app.use(express.static(__dirname + "/public"))

app.use("/api",api)

app.listen(config.port, () => console.log(`Example app listening on port ${config.port}!`))